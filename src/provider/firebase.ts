import { initializeApp } from 'firebase/app';
import { getFirestore, collection, Firestore } from 'firebase/firestore';

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
    apiKey: "AIzaSyB7bln4d_HfzaF7x_yQXBjoxQCyvHP-i5I",
    authDomain: "contact-c5756.firebaseapp.com",
    projectId: "contact-c5756",
    storageBucket: "contact-c5756.appspot.com",
    messagingSenderId: "724812717703",
    appId: "1:724812717703:web:2a4ded9754550165576222",
    measurementId: "G-EC52CXTZYF"
};

export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
